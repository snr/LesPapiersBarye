# Données structurées du projet "Les Papiers Barye"

![Représentation du corpus](https://pense.inha.fr/images/Barye-vue-corpus-1.png "")

Les Papiers Barye sont un ensemble de correspondances et de documents relatifs à l’activité du sculpteur et peintre Antoine-Louis Barye (1795-1875). Ces documents sont conservés dans le fonds d'archives Antoine-Louis Barye de la Bibliothèque de l'Institut National d'Histoire de l'Art, collections Jacques Doucet. Le fonds est composé de 350 documents (918 pages) et organisés en 6 collections:
* Correspondance du vivant d'Antoine-Louis Barye
* Documentation anonyme sur Antoine-Louis Barye
* Documents relatifs à l'activité de Barye de son vivant
* Documentation biographique sur Antoine-Louis Barye
* Documents relatifs au commerce et à l'exposition des œuvres de Barye après sa mort
* Documentation iconographique

Le fonds est catalogué dans le Catalogue en ligne des archives et des manuscrits de l'enseignement supérieur (Calames) à l'adresse: http://www.calames.abes.fr/pub/#details?id=FileId-2654

## Les données de transcription
Le dépôt des données de transcriptions du projet propose les segmentations et les transcriptions brutes (sans données d'enrichissement) des 918 pages, organisées par document et par collection et encodée avec le format [ALTO](https://fr.wikipedia.org/wiki/ALTO_(XML)) (Analysed Layout and Text Object). Les transcriptions ont été réalisées dans l'environnement Transkribus en 2020 et 2021 par Victor Claass (INHA, responsable scientifique), Justine Gain (INHA, Chargée d'étude et de recherche) et Suzanne Martin-Vigier (INHA, Chargée d'étude et de recherche)   

Les fichiers de numérisation des documents se trouvent sur le serveur IIIF de la bibliothèque numérique de l'INHA. Les URL des fichiers de numérisations correspondant à chaque page de transcription sont indiqués dans l'élément alto/Description/sourceImageInformation/fileIdentifier du fichier XML-ALTO.

https://gitlab.inha.fr/snr/LesPapiersBarye/-/tree/main/LesPapiersBarye-Transcriptions
